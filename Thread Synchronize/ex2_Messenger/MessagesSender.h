#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <mutex>
#include <thread>
#include <chrono>

class MessagesSender
{
public:
	MessagesSender(); // constructor

	/// <summary>
	/// function reads every 60 seconds from the file 'data.txt' and moves the text from the file to the messages vector
	/// </summary>
	void getNewMessages();

	/// <summary>
	/// function sends the messages to all the users (inside 'output.txt') 
	/// </summary>
	void sendMessages();

	/*
	 function runs the menu.
	 Options:
	 1 - sign in, add another user to the list
	 2 - sign out, remove a user from the list
	 3 - Connected users, prints the user list
	 4 - exit, exits the menu
	*/
	void menu();

private:
	std::set< std::string > _connectedUsers;
	std::vector< std::string > _messages;

	bool _done;

	/// <summary>
	/// function checks if the user is connected or not
	/// </summary>
	/// <param name="userName">the name we want to check</param>
	/// <returns>true - if user is connected, else, false.</returns>
	bool isUserConnected(const std::string userName) const;

	/// <summary>
	/// function prints the list of connected users
	/// </summary>
	void printConnectedUsers() const;

	/// <summary>
	/// function adds another user to the list
	/// </summary>
	void signIn();

	/// <summary>
	/// function removes a user from the list
	/// </summary>
	void signOut();
};