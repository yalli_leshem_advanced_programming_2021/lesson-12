#include "MessagesSender.h"

std::mutex userMutex;

std::mutex msgMutex;
std::condition_variable newMessagesAlert;

MessagesSender::MessagesSender()
{
	this->_connectedUsers.clear();
	this->_messages.clear();
	this->_done = false;
}

/// <summary>
/// function reads every 60 seconds from the file 'data.txt' and moves the text from the file to the messages vector
/// </summary>
void MessagesSender::getNewMessages()
{
	std::string newMessage;

	while (!this->_done) // run until user wants to exit
	{
		// open file for reading
		std::fstream dataFile;
		dataFile.open("data.txt", std::fstream::in);

		if (dataFile.is_open()) // make sure the file opened properly
		{
			// lock the access to the _messages vector
			std::unique_lock<std::mutex> msgLocker(msgMutex);

			while (std::getline(dataFile, newMessage))
			{
				this->_messages.push_back(newMessage);
			}
			// unlock mutex and notify that the vector is usable again
			msgLocker.unlock();
			newMessagesAlert.notify_one();

			dataFile.close();

			// clear file
			dataFile.open("data.txt", std::fstream::out | std::fstream::trunc);
			dataFile.close();
		}
			
		// check the file every 60 seconds (devide it by 10 so yo dont have to wait as long for the thread to exit
		for (int i = 0; i < 10; i++)
		{
			if (this->_done) // if true then user wants to exit
				break;
			std::this_thread::sleep_for(std::chrono::seconds(6));
		}
	}

	// do this so the sendMessages thread can finish as well
	this->_messages.push_back("done");
	newMessagesAlert.notify_one();
}

/// <summary>
/// function sends the messages to all the users (inside 'output.txt') 
/// </summary>
void MessagesSender::sendMessages()
{
	std::ofstream outputFile ("output.txt");
	
	while (!this->_done) // run until user wants to exit
	{
		// lock the access to the _messages vector
		std::unique_lock<std::mutex> msgLocker(msgMutex);
		newMessagesAlert.wait(msgLocker, [&]() { return !this->_messages.empty(); }); // spurious wake

		if (this->_done) // if true then user is done
		{
			outputFile.close();
			return;
		}

		// lock the access to the _connectedUsers vector
		std::unique_lock<std::mutex> userLocker(userMutex);

		for (std::string message : this->_messages) // go through all messages
		{
			for (std::string userName : this->_connectedUsers) // each message is sent to all users
			{
				outputFile << userName << ": " << message << std::endl;
			}
		}
		userLocker.unlock(); // unlock user list access

		this->_messages.clear(); // remove all the messages we just added
		msgLocker.unlock(); // unlock vector access
	}
	outputFile.close();
}

/*
 function runs the menu.
 Options:
 1 - sign in, add another user to the list
 2 - sign out, remove a user from the list
 3 - Connected users, prints the user list
 4 - exit, exits the menu
*/
void MessagesSender::menu()
{
	enum options { SIGN_IN = 1, SIGN_OUT, PRINT_USERS, EXIT_MENU };
	unsigned int choice = 0;

	while (choice != EXIT_MENU)
	{
		// print menu
		std::cout << "1 - SignIn" << std::endl;
		std::cout << "2 - SignOut" << std::endl;
		std::cout << "3 - Print connected users" << std::endl;
		std::cout << "4 - Exit" << std::endl;
		std::cout << "Please enter your choice: ";

		// get a valid choice
		std::cin >> choice;
		while (std::cin.fail() || !(SIGN_IN <= choice && choice <= EXIT_MENU))
		{
			std::cout << "Invalid Entry\nEnter 1-4" << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cin >> choice;
		}
		getchar(); //clear buffer

		switch (choice)
		{
		case SIGN_IN: // add another user to the user list
			this->signIn();
			break;

		case SIGN_OUT: // remove a user from the user list
			this->signOut();
			break;

		case PRINT_USERS: // print the user list
			this->printConnectedUsers();
			break;

		default: // exit menu
			std::cout << "Bye Bye! (wait about 6 seconds)" << std::endl;
			this->_done = true;
			break;
		}
	}
}

/// <summary>
/// function checks if the user is connected or not
/// </summary>
/// <param name="userName">the name we want to check</param>
/// <returns>true - if user is connected, else, false.</returns>
bool MessagesSender::isUserConnected(const std::string userName) const
{
	return (this->_connectedUsers.count(userName) != 0); // if not equal to zero then the user IS in the list
}

/// <summary>
/// function prints the list of connected users
/// </summary>
void MessagesSender::printConnectedUsers() const
{
	std::cout << "\nConnected users:" << std::endl;

	for (const std::string userName : this->_connectedUsers)
	{
		std::cout << userName << std::endl;
	}
}

/// <summary>
/// function adds another user to the list
/// </summary>
void MessagesSender::signIn()
{
	std::string userName;

	// get the user name
	std::cout << "\nEnter the new user's name: ";
	std::getline(std::cin, userName);

	if (this->isUserConnected(userName)) // if true, the user exists
	{
		std::cout << "User already connected.\n" << std::endl;
	}
	else // if user isn't in the list, add it
	{
		// lock the access to the _connectedUsers vector
		std::unique_lock<std::mutex> userLocker(userMutex);

		this->_connectedUsers.insert(userName);

		userLocker.unlock(); // unlock user list access
		std::cout << "User signed in successfully.\n" << std::endl;
	}
}

/// <summary>
/// function removes a user from the list
/// </summary>
void MessagesSender::signOut()
{
	std::string userName;

	// get the user name
	std::cout << "\nEnter the name of the user that wants to sign out: ";
	std::getline(std::cin, userName);

	// lock the access to the _connectedUsers vector
	std::unique_lock<std::mutex> userLocker(userMutex);

	if (this->_connectedUsers.erase(userName) == 0) // if zero, no user was deleted meaning the user wasn't in the list
	{
		std::cout << "User not found!\n" << std::endl;
	}
	else
	{
		std::cout << "User signed out successfully.\n" << std::endl;
	}

	userLocker.unlock(); // unlock user list access
}