#include <iostream>
#include "MessagesSender.h"

void callGetNewMessages(MessagesSender &msgObj);
void callSendMessages(MessagesSender &msgObj);

int main()
{
    MessagesSender msgObj;
 
    std::thread newMessagesThread (callGetNewMessages, std::ref(msgObj));
    std::thread sendMessagesThread (callSendMessages, std::ref(msgObj));

    msgObj.menu();

    newMessagesThread.join(); // wait for thread
    sendMessagesThread.join(); // wait for thread

    return 0;
}

void callGetNewMessages(MessagesSender &msgObj)
{
    msgObj.getNewMessages();
}

void callSendMessages(MessagesSender &msgObj)
{
    msgObj.sendMessages();
}