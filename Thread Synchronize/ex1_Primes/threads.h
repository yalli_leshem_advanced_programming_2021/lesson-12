#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>

void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);