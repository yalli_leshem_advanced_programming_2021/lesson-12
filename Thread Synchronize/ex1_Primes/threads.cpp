#include "threads.h"

std::mutex writer_mtx;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
    if (file.is_open()) // check that the file sent was properly open
    {
        bool isPrime = false;

        // Traverse each number in the interval with the help of for loop 
        for (int i = begin; i <= end; i++) {

            // Skip 0 and 1 as they are neither prime nor composite 
            if (i == 1 || i == 0)
                continue;

            // flag variable to tell if i is prime or not
            isPrime = true;

            // Iterate to check if i is prime or not
            for (int j = 2; j <= i / 2; ++j) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime)
            {
                writer_mtx.lock();
                file << i << "\n";
                writer_mtx.unlock();
            }
        }
    }
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
    // open file
    std::ofstream file;
    file.open(filePath);

    // get the amount of numbers each thread covers
    int slice = (end - begin) / N;
    std::vector<std::thread> multiThread;

    auto t1 = std::chrono::high_resolution_clock::now(); // start clock

    // begin threads
    for (int i = 0; i < N; i++)
    {
        // get the start of the slice the thread needs to check
        int beginSlice = begin + slice * i;

        if (i != 0) // the start of each slice (that isn't the first one) needs to start one after  
            beginSlice++;

        // get the end of the slice the thread needs to check
        int endSlice = begin + slice * (i + 1);

        if (i == N - 1) // if we are at the last thread, the end of the slice is the end
            endSlice = end;

        // make thread
        multiThread.push_back(std::move(std::thread(writePrimesToFile, beginSlice, endSlice, std::ref(file))));
    }

    // wait for threads to finish processing
    for (int i = 0; i < N; i++)
    {
        multiThread[i].join();
    }

    auto t2 = std::chrono::high_resolution_clock::now(); // stop clock 
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count(); // calculate duration of the threads
    std::cout << duration << "milliseconds" << std::endl; // print duration
}